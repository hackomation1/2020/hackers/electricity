# 1. Project Title: Smart vacation Services

This project gives any vacationer the unique posibility to control and manage electronic devices alongside services offered by the destination.
The controling will be done by a touch screen remote powered by the esp32 MCU.
Our plan is to make a vaction even more lazy for the vacationer.

## Overview & Features

**Client side:**
<ul>
    <li>Control of kitchen, living room appliances and lights</il>
    <li>control of airconditioning and other luxury electric appliances</il>
    <li>order room service</il>
    <li>bookings of on-terrain activities</il>
</ul>

**Adminitrator(s):**
<ul>
    <li>Overview of tourists staying and duration</li>
    <li>Backlog of client usage</li>
</ul>

## Demo / Proof of work

< Fotos, video of your working project as proof of work>

## Deliverables

< The deliverables of the project, so we know when it is complete>
- Project poster
- Proof of concept working
- Product pitch deck

## Project landing page/website (optional)

< if you created website or the like for your project link it up here>

## the team & contact

### Dinesh Harkhoe
<pre>
Student Electrical engineering - Information Sciences
Embedded systems engineer
skills: C, C++, Python, Java
Mob: 8564245
E-mail: dinharkhoe@gmail.com
</pre>

### Jordi Setroinangoen
<pre>
Student Electrical engineering - Information Sciences
Full stack developer
Skills: Java, Node.js, Postgresql
Mob: 8606401
E-mail: jorset98@gmail.com
</pre>

### Stefan Udit
<pre>
Student Electrical engineering - Information Sciences
Full stack developer, Machine learning
Skills: Java, C++, Python, R
Mob: 8990187
E-mail: pyrostef3@gmail.com
</pre>


### Ian Dhaukal
<pre>
Student Electrical engineering - Telecommunications engineering
Embedded systems, Electronic engineer
Skills: C++, MatLab, 
Mob: 8779792
E-mail: iandhaukal@gmail.com 
</pre>